import unittest

import dictdive

class TestDictDive(unittest.TestCase):

    data = {
        'objects': {
            'a': {
                'f1': 'abc',
                'f2': 'def',
            },
            'b': {
                'f1': 'ghi',
                'f2': 'jkl',
            },
        },
        'ignored': {
            'a': {
                'f1': 'abc',
                'f2': 'def',
            },
            'b': {
                'f1': 'ghi',
                'f2': 'jkl',
            },
        },
        'non-string keys': {
            1: {
                'f1': 'mno',
                'f2': 'pqr',
            },
            None: {
                'f1': 'stu',
                'f2': 'vwx',
            },
            ('id', 1): {
                'f1': 'yza',
                'f2': 'bcd',
            }
        },
    }

    def get_results(self, path):
        return tuple(dictdive.dive(self.data, path))

    def make_expected_item(self, crumbs, value):
        return (dictdive._key(crumbs), value)

    def make_expected(self, *crumbsitems):
        return tuple(self.make_expected_item(crumbs, values) for crumbs, values in crumbsitems)

    def test_dictdive(self):
        results = self.get_results(['objects', dictdive.wildcard, 'f1'])
        expected = self.make_expected(
            (['root', 'objects', 'a', 'f1'], 'abc'),
            (['root', 'objects', 'b', 'f1'], 'ghi'),
        )
        self.assertEqual(results, expected)
        # [ ] wildcard at beginning/end

    def test_None_keys(self):
        results = self.get_results(['non-string keys', None])
        expected = self.make_expected(
            (['root', 'non-string keys', None], {'f1': 'stu', 'f2': 'vwx'}),
        )
        self.assertEqual(results, expected)

        results = self.get_results(['non-string keys', None, 'f2'])
        expected = self.make_expected(
            (['root', 'non-string keys', None, 'f2'], 'vwx'),
        )
        self.assertEqual(results, expected)

        # wildcard at start
        results = self.get_results([dictdive.wildcard, None])
        expected = self.make_expected(
            (['root', 'non-string keys', None], {'f1': 'stu', 'f2': 'vwx'}),
        )
        self.assertEqual(results, expected)

        # wildcard at end
        results = self.get_results(['non-string keys', None, dictdive.wildcard])
        expected = self.make_expected(
            (['root', 'non-string keys', None, 'f1'], 'stu'),
            (['root', 'non-string keys', None, 'f2'], 'vwx'),
        )
        self.assertEqual(results, expected)

    def test_int_keys(self):
        results = self.get_results(['non-string keys', 1])
        expected = self.make_expected(
            (['root', 'non-string keys', 1], {'f1': 'mno', 'f2': 'pqr'}),
        )
        self.assertEqual(results, expected)

        results = self.get_results(['non-string keys', 1, 'f2'])
        expected = self.make_expected(
            (['root', 'non-string keys', 1, 'f2'], 'pqr'),
        )
        self.assertEqual(results, expected)

        # wildcard at start
        results = self.get_results([dictdive.wildcard, 1])
        expected = self.make_expected(
            (['root', 'non-string keys', 1], {'f1': 'mno', 'f2': 'pqr'}),
        )
        self.assertEqual(results, expected)

        # wildcard at end
        results = self.get_results(['non-string keys', 1, dictdive.wildcard])
        expected = self.make_expected(
            (['root', 'non-string keys', 1, 'f1'], 'mno'),
            (['root', 'non-string keys', 1, 'f2'], 'pqr'),
        )
        self.assertEqual(results, expected)

    def test_tuple_keys(self):
        key = ('id', 1)
        results = self.get_results(['non-string keys', key])
        expected = self.make_expected(
            (['root', 'non-string keys', key], {'f1': 'yza', 'f2': 'bcd'}),
        )
        self.assertEqual(results, expected)

        results = self.get_results(['non-string keys', key, 'f2'])
        expected = self.make_expected(
            (['root', 'non-string keys', key, 'f2'], 'bcd'),
        )
        self.assertEqual(results, expected)

        # wildcard at start
        results = self.get_results([dictdive.wildcard, key])
        expected = self.make_expected(
            (['root', 'non-string keys', key], {'f1': 'yza', 'f2': 'bcd'}),
        )
        self.assertEqual(results, expected)

        # wildcard at end
        results = self.get_results(['non-string keys', key, dictdive.wildcard])
        expected = self.make_expected(
            (['root', 'non-string keys', key, 'f1'], 'yza'),
            (['root', 'non-string keys', key, 'f2'], 'bcd'),
        )
        self.assertEqual(results, expected)

    def test_callable_search(self):
        def f(key):
            return isinstance(key, int)
        results = self.get_results(['non-string keys', f])
        expected = self.make_expected(
            (['root', 'non-string keys', 1], {'f1': 'mno', 'f2': 'pqr'}),
        )
        self.assertEqual(results, expected)

        results = self.get_results(['non-string keys', f, dictdive.wildcard])
        expected = self.make_expected(
            (['root', 'non-string keys', 1, 'f1'], 'mno'),
            (['root', 'non-string keys', 1, 'f2'], 'pqr'),
        )
        self.assertEqual(results, expected)

        def f(key):
            return isinstance(key, tuple)
        results = self.get_results(['non-string keys', f])
        expected = self.make_expected(
            (['root', 'non-string keys', ('id', 1)], { 'f1': 'yza', 'f2': 'bcd', }),
        )
        self.assertEqual(results, expected)

        results = self.get_results(['non-string keys', f, dictdive.wildcard])
        expected = self.make_expected(
            (['root', 'non-string keys', ('id', 1), 'f1'], 'yza'),
            (['root', 'non-string keys', ('id', 1), 'f2'], 'bcd'),
        )
        self.assertEqual(results, expected)


if __name__ == '__main__':
    unittest.main()
