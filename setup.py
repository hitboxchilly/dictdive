from setuptools import setup

setup(
    name = 'dictdive',
    version = '0.1',
    description = 'Dive into deeply nested dicts.',
    py_modules = ['dictdive']
)
