* [X] recursively dive into nested dicts with wildcard
* [X] support keys other than string
* [ ] support inner types other than dict
