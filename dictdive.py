_keysep = '->'
_keywrap = '[{}]'.format

def _key(args):
    return _keysep.join(map(_keywrap, args))

wildcard = object()

class Divekey:

    def __init__(self, obj):
        self.obj = obj

    def __eq__(self, other):
        if self.obj is wildcard:
            return True
        elif callable(self.obj):
            return self.obj(other)
        return self.obj == other


class Diver:

    def __init__(self, visitor=None):
        if visitor is None:
            visitor = self.visit
        self.visitor = visitor

    def visit(self, d, key, crumbs):
        return (_key(crumbs + [key]), d[key])

    def dive(self, d, path, _crumbs=None):
        """
        Recursively descend into d, until nothing remains in path, yielding
        matching keys.

        :param d: some object
        :param path: container holding keys to match on d
        """
        if _crumbs is None:
            _crumbs = type(path)(['root'])
        keydive, remaining = Divekey(path[0]), path[1:]
        for key in d:
            if keydive == key:
                if remaining:
                    yield from self.dive(d[key], remaining, _crumbs + type(path)([key]))
                else:
                    yield self.visitor(d, key, _crumbs)


def dive(d, path):
    diver = Diver()
    for thing in diver.dive(d, path):
        yield thing
